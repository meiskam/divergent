/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent.listener;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.shininet.bukkit.divergent.DivergentPlugin;
import org.shininet.bukkit.divergent.structure.Arena;
import org.shininet.bukkit.divergent.structure.Team;

/**
 * @author meiskam
 */
public class PlayerListener implements Listener {

    private DivergentPlugin plugin;

    public PlayerListener(DivergentPlugin plugin) {
        this.plugin = plugin;
    }

    public void registerEvents() {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Action action = event.getAction();

        if (action == Action.RIGHT_CLICK_BLOCK) {
            BlockState blockState = event.getClickedBlock().getState();
            if (blockState instanceof Sign) {
                Sign sign = (Sign) blockState;
                String signCommand = ChatColor.stripColor(sign.getLines()[0]);
                if (signCommand.equalsIgnoreCase("[join]")) {
                    String arenaName = ChatColor.stripColor(sign.getLines()[1]);
                    Arena arena = plugin.getArena(arenaName);
                    if (arena == null) {
                        plugin.getLogger().warning("Sign with non-existing arena clicked on at " + sign.getLocation());
                        player.sendMessage(ChatColor.RED + "That arena doesn't exist");
                        return;
                    }
                    plugin.playerJoinArena(player, arena);
                } else if (signCommand.equalsIgnoreCase("[leave]")) {
                    if (plugin.hasPermission(player, "divergent.leave")) {
                        plugin.playerLeaveArena(player, true);
                    } else {
                        player.sendMessage(ChatColor.RED + "You don't have permission.");
                    }
                } else if (signCommand.equalsIgnoreCase("[lobby]")) {
                    if (plugin.hasPermission(player, "divergent.lobby")) {
                        if (!plugin.playerLeaveArena(player, false)) {
                            plugin.playerToLobby(player, true);
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "You don't have permission.");
                    }
                } else if (signCommand.equalsIgnoreCase("[team]")) {
                    String teamName = ChatColor.stripColor(sign.getLines()[1]);
                    Team team = plugin.getTeam(teamName);
                    if (team == null) {
                        plugin.getLogger().warning("Sign with non-existing team clicked on at " + sign.getLocation());
                        player.sendMessage(ChatColor.RED + "That team doesn't exist");
                        return;
                    }
                    plugin.playerJoinTeam(player, team);
                }
            }
        }
    }

    @EventHandler(
            priority = EventPriority.HIGH)
    public void onPlayerDamage(EntityDamageEvent event) { //for death checking
        if (event.getEntityType() != EntityType.PLAYER) {
            return;
        }
        Player player = (Player) event.getEntity();
        if (player.getWorld() != plugin.getConfigManager().getWorld()) {
            return;
        }
        if (event.getDamage() < player.getHealth()) {
            return;
        }
        Arena arena = plugin.getPlayerArena(player);
        if ((arena != null) && (arena.isRunning())) {
            event.setCancelled(true);
            arena.messagePlayers(ChatColor.GOLD + player.getDisplayName() + " died");
            plugin.playerLeaveArena(player, true);
        }
    }

    @EventHandler
    public void onPlayerDamageByPlayer(EntityDamageByEntityEvent event) { //for damage modification
        if (event.getEntityType() != EntityType.PLAYER) {
            return;
        }
        Player player = (Player) event.getEntity();
        if (player.getWorld() != plugin.getConfigManager().getWorld()) { //we're checking if they're enrolled in a running arena later, checking worlds is moot ... nevermind
            return;
        }
        Entity entityDamager = event.getDamager();
        boolean projectile = false;
        if (entityDamager instanceof Projectile) {
            Projectile projectileEntity = ((Projectile) entityDamager);

            Object shooter = projectileEntity.getShooter();
            if (shooter instanceof Entity) {
                entityDamager = (Entity) shooter;
            }
            projectile = true;
        }
        if (!(entityDamager instanceof Player)) {
            return;
        }
        Player playerDamager = (Player) entityDamager;
        Arena arenaPlayer = plugin.getPlayerArena(player);
        Arena arenaDamager = plugin.getPlayerArena(playerDamager);
        if ((arenaPlayer == null) || (!arenaPlayer.isRunning()) || (arenaPlayer != arenaDamager)) {
            event.setCancelled(true); //wait, do we want this? //XXX
            return;
        }
        //arenaPlayer has to be set, arena has to be running, players have to be in the same arena

        Team teamDamager = plugin.getPlayerTeam(playerDamager);
        ArrayList<ItemStack> skillItems = teamDamager.getSkillItems();
        if (skillItems == null) {
            return;
        }
        ItemStack itemInHand = playerDamager.getItemInHand();
        Material itemInHandMaterial = null;
        if (itemInHand != null) {
            itemInHandMaterial = itemInHand.getType();
        }

        for (ItemStack skillItem : skillItems) {
            if (skillItem.getType() == Material.BOW) {
                if ((projectile) && (event.getDamager() instanceof Arrow)) {
                    //DivergentPlugin.debug(playerDamager, "You've done 20% extra damage with your arrow");
                    event.setDamage(event.getDamage() * 1.2); //increase damage by 20% for special weapons
                    return;
                }
            } else if (itemInHandMaterial == skillItem.getType()) {
                //DivergentPlugin.debug(playerDamager, "You've done 20% extra damage with your special weapon");
                event.setDamage(event.getDamage() * 1.2); //increase damage by 20% for special weapons
                return;
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        plugin.playerLeaveArena(player, false);
        plugin.playerLeaveTeam(player, false);
    }
}
