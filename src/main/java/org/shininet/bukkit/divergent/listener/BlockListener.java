/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent.listener;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.DoubleChestInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.shininet.bukkit.divergent.DivergentPlugin;
import org.shininet.bukkit.divergent.structure.Arena;

/**
 * @author meiskam
 */
public class BlockListener implements Listener {

    private DivergentPlugin plugin;

    public BlockListener(DivergentPlugin plugin) {
        this.plugin = plugin;
    }

    public void registerEvents() {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onBlockDamage(BlockDamageEvent event) {
        if (helper(event.getPlayer(), event.getBlock())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if (helper(event.getPlayer(), event.getBlock())) {
            event.setCancelled(true);
        }
    }

    /*@EventHandler
    public void onBucket(PlayerBucketEvent event) {
        Player player = event.getPlayer();
        if (helper(player)) {
            event.setCancelled(true);
        }
    }*/

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        if (helper(player, null)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onChestClose(InventoryCloseEvent event) {
        Inventory inventory = event.getInventory();
        if (inventory.getType() != InventoryType.CHEST) {
            return;
        }
        for (ItemStack itemStack : inventory.getContents()) {
            if (itemStack != null) {
                return;
            }
        }
        InventoryHolder holder = inventory.getHolder();
        Chest chest, chest2 = null;
        int viewerCount;
        if (inventory instanceof DoubleChestInventory) {
            viewerCount = ((DoubleChestInventory)inventory).getLeftSide().getViewers().size();
            chest = (Chest)((DoubleChest)holder).getLeftSide();
            chest2 = (Chest)((DoubleChest)holder).getRightSide();
        } else {
            viewerCount = inventory.getViewers().size();
            chest = (Chest)holder;
        }
        if (viewerCount > 1) {
            return;
        }
        Player player = (Player) event.getPlayer();
        Arena arena = plugin.getPlayerArena(player);
        if (chest.getLocation().getWorld() != plugin.getConfigManager().getWorld()) {
            return;
        }
        if ((arena == null) || (arena.isRunning() == false)) {
            return;
        }
        if (!((arena.getRegion().contains(plugin.locationToWEVector(chest.getLocation()))) && ((chest2 == null) || (arena.getRegion().contains(plugin.locationToWEVector(chest2.getLocation())))))) {
            return;
        }
        chest.setType(Material.AIR);
        chest.update(true);
        if (chest2 != null) {
            chest2.setType(Material.AIR);
            chest2.update(true);
        }
    }

    private boolean helper(Player player, Block block) {
        if (player.getWorld() != plugin.getConfigManager().getWorld()) {
            return false;
        }
        Arena arena = plugin.getPlayerArena(player);
        if ((arena == null) || (arena.isRunning() == false)) {
            if (plugin.hasPermission(player, "divergent.modifyworld")) {
                return false;
            }
            return true;
        }
        if (block == null) {
            return true;
        }
        if ((DivergentPlugin.setBlockBreakAllow.contains(block.getType())) && (arena.getRegion().contains(plugin.locationToWEVector(block.getLocation())))) {
            return false;
        }
        return true;
    }
}
