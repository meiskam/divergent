/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent.listener;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.kitteh.tag.AsyncPlayerReceiveNameTagEvent;
import org.shininet.bukkit.divergent.DivergentPlugin;
import org.shininet.bukkit.divergent.structure.Team;

/**
 * @author meiskam
 */
public class TagAPIListener implements Listener {

    private DivergentPlugin plugin;

    public TagAPIListener(DivergentPlugin plugin) {
        this.plugin = plugin;
    }

    public void registerEvents() {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onNameTag(AsyncPlayerReceiveNameTagEvent event) {
        Player player = event.getNamedPlayer();
        if (player.getWorld() != plugin.getConfigManager().getWorld()) {
            return;
        }
        Team team = plugin.getPlayerTeam(player);
        if (team == null) {
            return;
        }
        ChatColor color = team.getColor();
        if (color == null) {
            return;
        }
        event.setTag(color + player.getName());
    }
}
