/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.kitteh.tag.TagAPI;
import org.shininet.bukkit.divergent.command.GeneralCommands;
import org.shininet.bukkit.divergent.listener.BlockListener;
import org.shininet.bukkit.divergent.listener.PlayerListener;
import org.shininet.bukkit.divergent.listener.TagAPIListener;
import org.shininet.bukkit.divergent.structure.ChestGroup;
import org.shininet.bukkit.divergent.structure.Team;
import org.shininet.bukkit.divergent.structure.Arena;

import com.sk89q.bukkit.util.CommandsManagerRegistration;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissionsException;
import com.sk89q.minecraft.util.commands.CommandUsageException;
import com.sk89q.minecraft.util.commands.CommandsManager;
import com.sk89q.minecraft.util.commands.MissingNestedCommandException;
import com.sk89q.minecraft.util.commands.SimpleInjector;
import com.sk89q.minecraft.util.commands.WrappedCommandException;
import com.sk89q.wepif.PermissionsResolverManager;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.LocalWorld;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;

/**
 * @author sk89q
 * @author meiskam
 */
public class DivergentPlugin extends JavaPlugin {

    private WorldEditPlugin worldEditPlugin;
    private EditSession session;
    private File arenaFolder;
    private HashMap<String, String> mapPlayerArena;
    private HashMap<String, String> mapPlayerTeam;
    private Random prng;
    public static boolean debug = true;
    public static final Set<Material> setBlockBreakAllow = new HashSet<Material>(Arrays.asList(Material.LEAVES, Material.LEAVES_2, Material.DEAD_BUSH, Material.DOUBLE_PLANT, Material.LONG_GRASS));

    /**
     * Current instance of this plugin.
     */
    private static DivergentPlugin inst;

    /**
     * Manager for commands. This automatically handles nested commands,
     * permissions checking, and a number of other fancy command things.
     * We just set it up and register commands against it.
     */
    private final CommandsManager<CommandSender> commands;

    /**
     * Handles all configuration.
     */
    private final ConfigurationManager config;

    /**
     * Construct objects. Actual loading occurs when the plugin is enabled, so
     * this merely instantiates the objects.
     */
    public DivergentPlugin() {
        config = new ConfigurationManager(this);
        prng = new Random();

        final DivergentPlugin plugin = inst = this;
        commands = new CommandsManager<CommandSender>() {
            @Override
            public boolean hasPermission(CommandSender player, String perm) {
                return plugin.hasPermission(player, perm);
            }
        };
    }

    /**
     * Get the current instance of DivergentPlugin
     * 
     * @return DivergentPlugin instance
     */
    public static DivergentPlugin inst() {
        return inst;
    }

    /**
     * Called on plugin enable.
     */
    @Override
    public void onEnable() {
        try {
            // Check plugin for checking the active states of a plugin
            Plugin checkPlugin;

            // Check for WorldEdit
            checkPlugin = getServer().getPluginManager().getPlugin("WorldEdit");
            if (checkPlugin != null && checkPlugin instanceof WorldEditPlugin) {
                worldEditPlugin = (WorldEditPlugin) checkPlugin;
            } else {
                getLogger().severe("WorldEdit detection has failed!");
                getLogger().severe("WorldEdit is a required dependency, Divergent disabled!");
                setEnabled(false);
                return;
            }

            // Setup proper serialization for our classes
            ConfigurationSerialization.registerClass(org.shininet.bukkit.divergent.structure.Team.class);
            ConfigurationSerialization.registerClass(org.shininet.bukkit.divergent.structure.ChestGroup.class);
            ConfigurationSerialization.registerClass(org.shininet.bukkit.divergent.structure.Arena.class);
            ConfigurationSerialization.registerClass(org.shininet.bukkit.divergent.structure.Location.class);

            // Set the proper command injector
            commands.setInjector(new SimpleInjector(this));

            // Register command classes
            final CommandsManagerRegistration reg = new CommandsManagerRegistration(this, commands);
            reg.register(GeneralCommands.class);

            // Create the plugins/Divergent/arenas folder
            arenaFolder = new File(getDataFolder(), "arenas");
            arenaFolder.mkdirs();

            PermissionsResolverManager.initialize(this);

            // Load the configuration
            config.loadAll();

            // Register events
            (new PlayerListener(this)).registerEvents();
            (new BlockListener(this)).registerEvents();
            (new TagAPIListener(this)).registerEvents();

            mapPlayerArena = new HashMap<String, String>();
            mapPlayerTeam = new HashMap<String, String>();
        } catch (Exception e) {
            getLogger().severe("Exception while enabling");
            e.printStackTrace();
            setEnabled(false);
        }
    }

    /**
     * Called on plugin disable.
     */
    @Override
    public void onDisable() {
        stopAllArenas();
        this.getServer().getScheduler().cancelTasks(this);
    }

    /**
     * Handle a command.
     */
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        try {
            commands.execute(cmd.getName(), args, sender, sender);
        } catch (CommandPermissionsException e) {
            sender.sendMessage(ChatColor.RED + "You don't have permission.");
        } catch (MissingNestedCommandException e) {
            sender.sendMessage(ChatColor.RED + e.getUsage());
        } catch (CommandUsageException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
            sender.sendMessage(ChatColor.RED + e.getUsage());
        } catch (WrappedCommandException e) {
            if (e.getCause() instanceof NumberFormatException) {
                sender.sendMessage(ChatColor.RED + "Number expected, string received instead.");
            } else {
                sender.sendMessage(ChatColor.RED + "An error has occurred. See console.");
                e.printStackTrace();
            }
        } catch (CommandException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
        }

        return true;
    }

    //get-set
    /**
     * Get the global ConfigurationManager.
     * Use this to access global configuration values.
     * 
     * @return The global ConfigurationManager
     */
    public ConfigurationManager getConfigManager() {
        return config;
    }

    public WorldEditPlugin getWorldEditPlugin() {
        return worldEditPlugin;
    }

    public File getArenaFolder() {
        return arenaFolder;
    }

    public EditSession getSession() {
        return session;
    }

    //dynamic get
    public LocalWorld getLocalWorld() {
        World worldBukkit = Bukkit.getWorld(config.getWorldName());
        if (worldBukkit == null)
            return null;
        return BukkitUtil.getLocalWorld(worldBukkit);
    }

    //methods
    public void onConfigLoad() {
        session = new EditSession(getLocalWorld(), config.getMaxArenaSize());
    }

    public Arena getArena(String arenaName) {
        for (Arena arena : getConfigManager().getArenas()) {
            if (arena.getName().equalsIgnoreCase(arenaName)) {
                return arena;
            }
        }
        return null;
    }

    public Team getTeam(String teamName) {
        for (Team team : getConfigManager().getTeams()) {
            if (team.getName().equalsIgnoreCase(teamName)) {
                return team;
            }
        }
        return null;
    }

    public Arena wherePlayer(Player player) {
        if (player.getWorld() != getConfigManager().getWorld()) {
            return null;
        }
        com.sk89q.worldedit.Vector vector = locationToWEVector(player.getLocation());
        for (Arena arena : getConfigManager().getArenas()) {
            if (arena.getRegion().contains(vector)) {
                return arena;
            }
        }
        return null;
    }

    public Arena wherePlayer(String playerName) {
        Player player = Bukkit.getPlayer(playerName);
        if (player == null) {
            return null;
        }
        return wherePlayer(player);
    }

    public com.sk89q.worldedit.Vector locationToWEVector(org.bukkit.Location location) {
        return new com.sk89q.worldedit.Vector(location.getX(), location.getY(), location.getZ());
    }

    public com.sk89q.worldedit.Vector locationToWEVector(org.shininet.bukkit.divergent.structure.Location location) {
        return new com.sk89q.worldedit.Vector(location.getX(), location.getY(), location.getZ());
    }

    public void playerJoinArena(Player player, Arena arena) {
        if (getPlayerArena(player) != null) {
            player.sendMessage(ChatColor.RED + "You're already in an arena");
            return;
        }
        if (!hasPermission(player, "divergent.join." + arena.getName())) {
            player.sendMessage(ChatColor.RED + "You don't have permission to join arena " + arena.getName() + ".  Donate by doing /buy to join this arena.");
            return;
        }
        if (!arena.isEnabled()) {
            player.sendMessage(ChatColor.RED + "Arena " + arena.getName() + " is not available to join");
            return;
        }
        if (arena.isRunning()) {
            player.sendMessage(ChatColor.RED + "A game is currently running in " + arena.getName());
            return;
        }
        if (arena.getPlayers().size() >= getConfigManager().getPlayerMax()) {
            player.sendMessage(ChatColor.RED + arena.getName() + " is currently full");
            return;
        }
        if (arena.addPlayer(player)) {
            player.sendMessage(ChatColor.GREEN + "You joined arena " + arena.getName());
        } else {
            player.sendMessage(ChatColor.RED + "Error joining arena");
        }
    }

    public void playerJoinArena2(Player player, Arena arena) {
        mapPlayerArena.put(player.getName(), arena.getName());
    }

    public boolean playerLeaveArena(Player player, boolean message) {
        Arena arena = getPlayerArena(player);
        if (arena == null) {
            if (message) {
                player.sendMessage(ChatColor.RED + "You're not in an arena");
            }
            //playerToLobby(player); //XXX do we want this?
            return false;
        }
        if (arena.removePlayer(player, false)) {
            if (message) {
                player.sendMessage(ChatColor.GREEN + "You have left the arena");
            }
            return true;
        } else if (message) {
            player.sendMessage(ChatColor.RED + "Error leaving arena");
        }
        return false;
    }

    public boolean playerLeaveArena2(Player player, boolean message) {
        if (mapPlayerArena.remove(player.getName()) == null) {
            return false;
        }
        resetPlayer(player);
        playerToLobby(player, message);
        return true;
    }

    public void playerToLobby(Player player, boolean message) {
        org.shininet.bukkit.divergent.structure.Location lobby = getConfigManager().getLobbySpawn();
        if (lobby == null) {
            getLogger().warning("Global lobby spawn is not set");
            if (message) {
                player.sendMessage(ChatColor.RED + "Error bringing you to the lobby");
            }
            return;
        }
        org.bukkit.Location lobbyBukkit = lobby.toBukkit();
        if (player.getLocation() == lobbyBukkit) {
            return;
        }
        player.teleport(lobbyBukkit);
        if (message) {
            player.sendMessage(ChatColor.GOLD + "Welcome to the lobby");
        }
    }

    public Arena getPlayerArena(String playerName) {
        Player player = Bukkit.getPlayer(playerName);
        if (player == null) {
            return null;
        }
        return getPlayerArena(player);
    }

    public Arena getPlayerArena(Player player) {
        if (!mapPlayerArena.containsKey(player.getName())) {
            return null;
        }
        return getArena(mapPlayerArena.get(player.getName()));
    }

    @SuppressWarnings("deprecation")
    public void resetPlayer(Player player) {
        PlayerInventory inv = player.getInventory();

        inv.clear();
        inv.setArmorContents(null);
        player.updateInventory();
        player.setExp(0);
        player.setLevel(0);
        player.setHealth(player.getMaxHealth());
        player.setFoodLevel(20);
        player.setExhaustion(0);
        player.setSaturation(5);
        player.setRemainingAir(player.getMaximumAir());
        player.setFireTicks(0);

        for (PotionEffect pe : player.getActivePotionEffects()) {
            player.removePotionEffect(pe.getType());
        }
    }

    public void playerJoinTeam(Player player, Team team) {
        if (!hasPermission(player, "divergent.team." + team.getName())) {
            player.sendMessage(ChatColor.RED + "You don't have permission to join team " + team.getName() + ".  Donate by doing /buy to join this team.");
            return;
        }
        Arena arena = getPlayerArena(player);
        if (arena != null && arena.isRunning()) {
            player.sendMessage(ChatColor.RED + "You can not change your team while in an arena");
            return;
        }
        Team teamOld = getPlayerTeam(player);
        if (teamOld == team) {
            player.sendMessage(ChatColor.RED + "You are already in that team");
            return;
        }
        if (teamOld != null) {
            teamOld.removePlayer(player);
        }
        team.addPlayer(player);
        player.sendMessage(ChatColor.GREEN + "You joined team " + team.getName());
        if (player.getWorld() == getConfigManager().getWorld()) {
            TagAPI.refreshPlayer(player);
        }
    }

    public void playerJoinTeam2(Player player, Team team) {
        mapPlayerTeam.put(player.getName(), team.getName());
    }

    public boolean playerLeaveTeam(Player player, boolean message) {
        Team team = getPlayerTeam(player);
        if (team == null) {
            if (message) {
                player.sendMessage(ChatColor.RED + "You're not in a team");
            }
            return false;
        }
        Arena arena = getPlayerArena(player);
        if (arena != null && !arena.isRunning()) {
            if (message) {
                player.sendMessage(ChatColor.RED + "You can not change your team while in an arena");
            }
            return false;
        }
        if (!team.removePlayer(player)) {
            if (message) {
                player.sendMessage(ChatColor.RED + "Error leaving team");
            }
            return false;
        }
        if (message) {
            player.sendMessage(ChatColor.GREEN + "You have left your team");
        }
        if (player.getWorld() == getConfigManager().getWorld()) {
            TagAPI.refreshPlayer(player);
        }
        return true;
    }

    public boolean playerLeaveTeam2(Player player) {
        Arena arena = getPlayerArena(player);
        if ((arena != null) && (arena.isRunning())) {
            playerLeaveArena(player, false);
        }
        if (mapPlayerTeam.remove(player.getName()) == null) {
            return false;
        }
        return true;
    }

    public Team getPlayerTeam(String playerName) {
        Player player = Bukkit.getPlayer(playerName);
        if (player == null) {
            return null;
        }
        return getPlayerTeam(player);
    }

    public Team getPlayerTeam(Player player) {
        if (!mapPlayerTeam.containsKey(player.getName())) {
            return null;
        }
        return getTeam(mapPlayerTeam.get(player.getName()));
    }

    public ItemStack getRandChestItem() {
        ArrayList<ChestGroup> chests = getConfigManager().getChests();

        if ((chests == null) || (chests.size() == 0)) {
            return null;
        }
        if (chests.size() == 1) {
            return chests.get(0).getRandItem(prng);
        }

        double totalWeight = 0;
        for (ChestGroup chest : chests) {
            totalWeight += chest.getRate();
        }
        double randChestWeight = prng.nextDouble() * totalWeight;
        totalWeight = 0;
        for (ChestGroup chest : chests) {
            totalWeight += chest.getRate();
            if (totalWeight >= randChestWeight) {
                return chest.getRandItem(prng);
            }
        }

        return null;
    }

    public static void debug(Player player, String message) {
        if (debug) {
            if (player == null) {
                inst().getLogger().info(ChatColor.GOLD + "[DEBUG] " + message);
            } else {
                player.sendMessage(ChatColor.GOLD + "[Divergent:debug] " + message);
            }
        }
    }

    public void stopAllArenas() {
        for (Arena arena : getConfigManager().getArenas()) {
            arena.setRunning(false, "All matches have been stopped");
        }
    }

    public void leaveAllTeams() {
        stopAllArenas();
        for (Team team : getConfigManager().getTeams()) {
            team.bootPlayers(null);
        }
    }

    //static
    /**
     * Check whether a player is in a group.
     * This calls the corresponding method in PermissionsResolverManager
     * 
     * @param player The player to check
     * @param group The group
     * @return whether {@code player} is in {@code group}
     */
    public boolean inGroup(Player player, String group) {
        try {
            return PermissionsResolverManager.getInstance().inGroup(player, group);
        } catch (Throwable t) {
            t.printStackTrace();
            return false;
        }
    }

    /**
     * Get the groups of a player.
     * This calls the corresponding method in PermissionsResolverManager.
     * 
     * @param player The player to check
     * @return The names of each group the player is in.
     */
    public String[] getGroups(Player player) {
        try {
            return PermissionsResolverManager.getInstance().getGroups(player);
        } catch (Throwable t) {
            t.printStackTrace();
            return new String[0];
        }
    }

    /**
     * Gets the name of a command sender. This is a unique name and this
     * method should never return a "display name".
     * 
     * @param sender The sender to get the name of
     * @return The unique name of the sender.
     */
    public String toUniqueName(CommandSender sender) {
        if (sender instanceof ConsoleCommandSender) {
            return "*Console*";
        } else {
            return sender.getName();
        }
    }

    /**
     * Gets the name of a command sender. This may be a display name.
     * 
     * @param sender The CommandSender to get the name of.
     * @return The name of the given sender
     */
    public String toName(CommandSender sender) {
        if (sender instanceof ConsoleCommandSender) {
            return "*Console*";
        } else if (sender instanceof Player) {
            return ((Player) sender).getDisplayName();
        } else {
            return sender.getName();
        }
    }

    /**
     * Checks permissions.
     * 
     * @param sender The sender to check the permission on.
     * @param perm The permission to check the permission on.
     * @return whether {@code sender} has {@code perm}
     */
    public boolean hasPermission(CommandSender sender, String perm) {
        if (sender.isOp()) {
            if (sender instanceof Player) {
                if (config.isOpPermissions()) {
                    return true;
                }
            } else {
                return true;
            }
        }

        // Invoke the permissions resolver
        if (sender instanceof Player) {
            Player player = (Player) sender;
            return PermissionsResolverManager.getInstance().hasPermission(player.getWorld().getName(), player.getName(), perm);
        }

        return false;
    }

    /**
     * Checks permissions and throws an exception if permission is not met.
     * 
     * @param sender The sender to check the permission on.
     * @param perm The permission to check the permission on.
     * @throws CommandPermissionsException if {@code sender} doesn't have {@code perm}
     */
    public void checkPermission(CommandSender sender, String perm) throws CommandPermissionsException {
        if (!hasPermission(sender, perm)) {
            throw new CommandPermissionsException();
        }
    }

    /**
     * Checks to see if the sender is a player, otherwise throw an exception.
     * 
     * @param sender The {@link CommandSender} to check
     * @return {@code sender} casted to a player
     * @throws CommandException if {@code sender} isn't a {@link Player}
     */
    public Player checkPlayer(CommandSender sender) throws CommandException {
        if (sender instanceof Player) {
            return (Player) sender;
        } else {
            throw new CommandException("A player is expected.");
        }
    }
}
