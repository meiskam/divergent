/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent.structure;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.shininet.bukkit.divergent.DivergentPlugin;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.schematic.SchematicFormat;

/**
 * @author meiskam
 */
public class Arena implements ConfigurationSerializable {
    private String name;
    private boolean enabled = false;
    private ArrayList<org.shininet.bukkit.divergent.structure.Location> spawns;
    private org.shininet.bukkit.divergent.structure.Location lobby;

    private Random prng;
    private CuboidClipboard cuboid = null;
    private CuboidRegion region = null;
    private File file;
    private HashSet<String> players = new HashSet<String>();
    private boolean running = false;
    private BukkitRunnable startTask;

    //private boolean joinable = false;

    //constructors
    public Arena(String name, CuboidClipboard cuboid) {
        super();
        this.name = name;
        setCuboid(cuboid);
        this.spawns = new ArrayList<org.shininet.bukkit.divergent.structure.Location>();

        this.prng = new Random();
    }

    @SuppressWarnings("unchecked")
    public Arena(Map<String, Object> input) {
        super();
        this.name = (String) input.get("name");
        this.enabled = (Boolean) input.get("enabled");
        this.spawns = (ArrayList<org.shininet.bukkit.divergent.structure.Location>) input.get("spawns");
        this.lobby = (org.shininet.bukkit.divergent.structure.Location) input.get("lobby");
        loadCuboidRegion();
        if (this.cuboid == null) {
            this.enabled = false;
        }

        this.prng = new Random();
    }

    public void copy(Arena arenaNew) {
        String newName = arenaNew.getName();
        ArrayList<Location> newSpawns = arenaNew.getSpawns();
        boolean newEnabled = arenaNew.isEnabled();
        Location newLobby = arenaNew.getLobby();
        CuboidClipboard newCuboid = arenaNew.getCuboid();
        CuboidRegion newRegion = arenaNew.getRegion();

        boolean bootPlayers = false;
        if (!newName.equals(getName())) {
            delete();
            bootPlayers = true;
        }
        if (!newEnabled) {
            bootPlayers = true;
        }
        if ((newCuboid.getOrigin() != getCuboid().getOrigin()) || newCuboid.getSize() != getCuboid().getSize()) { //this isn't a perfect "equals" check, but .. meh
            bootPlayers = true;
        }

        if (bootPlayers) {
            bootPlayers("Arena has been updated");
            running = false;
            cancelStartTask();
        }

        name = newName;
        spawns = newSpawns;
        lobby = newLobby;
        cuboid = newCuboid;
        region = newRegion;
        enabled = newEnabled;

        saveCuboid(); //on case-insensitive filesystems we might have deleted the schematic, so let's make sure it's still there
    }

    //get-set
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean setEnabled(boolean enabled) {
        if ((this.enabled) && (!enabled)) {
            setRunning(false, "This arena is being disabled");
        } else {
            if ((cuboid == null) || (region == null) || (spawns.size() == 0) || (lobby == null)) {
                return false;
            }
        }
        this.enabled = enabled;
        return true;
    }

    public CuboidClipboard getCuboid() {
        return cuboid;
    }

    public void setCuboid(CuboidClipboard cuboid) {
        this.cuboid = cuboid;
        saveCuboid();
        loadRegion();
    }

    public CuboidRegion getRegion() {
        return region;
    }

    public ArrayList<org.shininet.bukkit.divergent.structure.Location> getSpawns() {
        return spawns;
    }

    public void setSpawns(ArrayList<org.shininet.bukkit.divergent.structure.Location> spawns) {
        this.spawns = spawns;
    }

    public org.shininet.bukkit.divergent.structure.Location getLobby() {
        return lobby;
    }

    public void setLobby(org.shininet.bukkit.divergent.structure.Location lobby) {
        this.lobby = lobby;
    }

    public HashSet<String> getPlayers() {
        return players;
    }

    public void setRunning(boolean running) {
        setRunning(running, null);
    }

    public void setRunning(boolean running, String message) {
        if ((this.running) && (!running)) {
            this.running = false;
            bootPlayers(message);
            reset();
        } else {
            if (players.size() <= 1) {
                messagePlayers(ChatColor.GOLD + "Arena not starting, not enough players");
            } else {
                this.running = true;
                start();
            }
        }
        cancelStartTask();
    }

    public boolean isRunning() {
        return running;
    }

    //methods
    private void loadCuboidRegion() { //TODO set origin from config, not schematic
        this.file = new File(DivergentPlugin.inst().getArenaFolder(), name + ".schematic");
        try {
            cuboid = SchematicFormat.MCEDIT.load(file);
        } catch (IOException e) {
            Bukkit.getLogger().warning("[Divergent] Error loading schematic " + file.getName());
            e.printStackTrace();
            cuboid = null;
            return;
        } catch (DataException e) {
            Bukkit.getLogger().warning("[Divergent] Error loading schematic " + file.getName());
            e.printStackTrace();
            cuboid = null;
            return;
        }
        loadRegion();
    }

    private void saveCuboid() { //TODO save cuboid origin into config
        File file = new File(DivergentPlugin.inst().getArenaFolder(), name + ".schematic");
        try {
            SchematicFormat.MCEDIT.save(cuboid, file);
        } catch (IOException e) {
            Bukkit.getLogger().warning("[Divergent] Error saving schematic " + file.getName());
            e.printStackTrace();
            return;
        } catch (DataException e) {
            Bukkit.getLogger().warning("[Divergent] Error saving schematic " + file.getName());
            e.printStackTrace();
            return;
        }
    }

    private void loadRegion() {
        com.sk89q.worldedit.Vector pos1 = cuboid.getOrigin();
        this.region = new CuboidRegion(pos1, pos1.add(cuboid.getSize()));

        //this.region.setWorld(world)
    }

    public void bootPlayers(String message) {
        Iterator<String> iter = players.iterator();
        while (iter.hasNext()) {
            String playerName = iter.next();
            Player player = Bukkit.getPlayer(playerName);
            if (player == null) {
                iter.remove();
                continue;
            }
            if (message != null) {
                player.sendMessage(ChatColor.GOLD + message);
            }
            DivergentPlugin.inst().playerLeaveArena2(player, false);
            iter.remove();
        }
    }

    public void reset() {
        try {
            cuboid.paste(DivergentPlugin.inst().getSession(), cuboid.getOrigin(), false);
        } catch (MaxChangedBlocksException e) {
            Bukkit.getLogger().warning("[Divergent] max-arena-size violated, increase the config or make a smaller arena");
        }
    }

    public void delete() {
        this.file = new File(DivergentPlugin.inst().getArenaFolder(), name + ".schematic");
        file.delete();
    }

    private Location randSpawn() {
        return getSpawns().get(prng.nextInt(getSpawns().size()));
    }

    public boolean addPlayer(Player player) {
        if ((!isEnabled()) || (isRunning()) || (players.size() >= DivergentPlugin.inst().getConfigManager().getPlayerMax())) {
            return false;
        }
        players.add(player.getName());
        DivergentPlugin.inst().playerJoinArena2(player, this);
        DivergentPlugin.inst().resetPlayer(player);
        player.teleport(getLobby().toBukkit());
        tryStart();
        return true;
    }

    public boolean removePlayer(Player player, boolean message) {
        if (!players.remove(player.getName())) {
            return false;
        }
        checkWin();

        return DivergentPlugin.inst().playerLeaveArena2(player, message);
    }

    public boolean checkWin() {
        if (players.size() > 1) {
            return false;
        }
        if (players.size() == 1) {
            Player player = Bukkit.getPlayer(players.toArray(new String[1])[0]);
            player.sendMessage(ChatColor.GOLD + "You Win");
            DivergentPlugin.inst().playerLeaveArena(player, true);
            return true;
        }
        setRunning(false);
        return false;
    }

    public boolean tryStart() {
        if ((isRunning()) || (!isEnabled())) {
            return false;
        }
        if (players.size() >= DivergentPlugin.inst().getConfigManager().getPlayerMin()) {
            startTimer(30, false);
            return true;
        }
        return false;
    }

    public boolean startTimer(int seconds, boolean override) {
        if ((isRunning()) || (!isEnabled())) {
            return false;
        }
        if (startTask != null) {
            if (override) {
                startTask.cancel();
            } else {
                return false;
            }
        }

        if (seconds > 0) {
            messagePlayers(ChatColor.GOLD + "Match starting in " + seconds + " seconds");
            startTask = new BukkitRunnable() {
                @Override
                public void run() {
                    setRunning(true);
                }
            };
            startTask.runTaskLater(DivergentPlugin.inst(), 20 * seconds);
        } else {
            setRunning(true);
        }
        return true;
    }

    private void cancelStartTask() {
        if (startTask != null) {
            startTask.cancel();
            startTask = null;
        }
    }

    public void messagePlayers(String message) {
        Player player;
        for (String playerName : players) {
            player = Bukkit.getPlayer(playerName);
            if (player == null) {
                return;
            }
            player.sendMessage(message);
        }
    }

    private void start() {
        if (!this.enabled) {
            return;
        }
        reset();
        String playerName;
        Player player;
        Team team;
        Iterator<String> iter = players.iterator();

        while (iter.hasNext()) {
            playerName = iter.next();
            player = Bukkit.getPlayer(playerName);
            team = DivergentPlugin.inst().getPlayerTeam(player);
            if (team == null) {
                iter.remove();
                player.sendMessage(ChatColor.RED + "You must be in a team to play a match");
                DivergentPlugin.inst().playerLeaveArena2(player, true);
                continue;
            }
            DivergentPlugin.inst().resetPlayer(player);
            ItemStack[] kitItems = team.getKitItemsArray();
            if (kitItems != null) {
                player.getInventory().addItem(team.getKitItemsArray());
            }
            player.teleport(randSpawn().toBukkit());
        }

        fillChests();

        messagePlayers(ChatColor.GOLD + "Match has started, good luck");

        checkWin();
    }

    private void fillChests() {
        com.sk89q.worldedit.Vector pointMin = getRegion().getMinimumPoint();
        com.sk89q.worldedit.Vector pointMax = getRegion().getMaximumPoint();
        int minX, minY, minZ, maxX, maxY, maxZ;

        minX = pointMin.getBlockX();
        minY = pointMin.getBlockY();
        minZ = pointMin.getBlockZ();
        maxX = pointMax.getBlockX();
        maxY = pointMax.getBlockY();
        maxZ = pointMax.getBlockZ();

        org.bukkit.Location location = new org.bukkit.Location(DivergentPlugin.inst().getConfigManager().getWorld(), minX, minY, minZ);
        Chest chestState;
        ItemStack itemStack;
        Inventory inventory;
        int maxChestItems = DivergentPlugin.inst().getConfigManager().getMaxChestItems();
        int maxChestItemsHalf = maxChestItems / 2;
        int chestItemsAmount;
        for (int x = minX; x <= maxX; x++) {
            location.setX(x);
            for (int z = minZ; z <= maxZ; z++) {
                location.setZ(z);
                for (int y = minY; y <= maxY; y++) {
                    location.setY(y);
                    if (location.getBlock().getType() == Material.CHEST) {
                        chestState = (Chest) location.getBlock().getState();
                        inventory = chestState.getBlockInventory();

                        chestItemsAmount = prng.nextInt(maxChestItemsHalf) + maxChestItemsHalf;
                        for (int i = 0; i < chestItemsAmount; i++) {
                            itemStack = DivergentPlugin.inst().getRandChestItem();
                            if (itemStack != null) {
                                inventory.addItem(itemStack);
                            }
                        }

                        chestState.update();
                    }
                }
            }
        }
    }

    public void updateCuboid() {
        cuboid.copy(DivergentPlugin.inst().getSession());
        saveCuboid();
        loadRegion();
    }

    //serialization
    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> output = new HashMap<String, Object>();
        output.put("name", name);
        output.put("enabled", enabled);
        output.put("spawns", spawns);
        output.put("lobby", lobby);
        return output;
    }

    public static Arena deserialize(Map<String, Object> input) {
        return new Arena(input);
    }

    public static Arena valueOf(Map<String, Object> input) {
        return new Arena(input);
    }
}
