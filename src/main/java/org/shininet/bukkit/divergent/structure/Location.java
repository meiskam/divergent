/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent.structure;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.shininet.bukkit.divergent.DivergentPlugin;

/**
 * @author meiskam
 */
public class Location implements ConfigurationSerializable {
    public static DecimalFormat df = new DecimalFormat("#.00");

    private double x;
    private double y;
    private double z;
    private double yaw;
    private double pitch;

    //constructors
    public Location(double x, double y, double z) {
        this(x, y, z, 0, 0);
    }

    public Location(double x, double y, double z, double yaw, double pitch) {
        super();
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public Location(org.bukkit.Location location) {
        this(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
    }

    public Location(com.sk89q.worldedit.Vector location) {
        this(location.getX(), location.getY(), location.getZ());
    }

    public Location(Map<String, Object> input) {
        super();
        try {
            this.x = (Double) input.get("x");
            this.y = (Double) input.get("y");
            this.z = (Double) input.get("z");
            this.yaw = (Double) input.get("yaw");
            this.pitch = (Double) input.get("pitch");
        } catch (Exception e) {
            Bukkit.getLogger().warning("[Divergent] Error deserializing Location");
            e.printStackTrace();
        }
    }

    //get-set
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public double getYaw() {
        return yaw;
    }

    public void setYaw(double yaw) {
        this.yaw = yaw;
    }

    public double getPitch() {
        return pitch;
    }

    public void setPitch(double pitch) {
        this.pitch = pitch;
    }

    //methods
    public org.bukkit.Location toBukkit() {
        return new org.bukkit.Location(DivergentPlugin.inst().getConfigManager().getWorld(), x, y, z, (float) yaw, (float) pitch);
    }

    public String toString() {
        return df.format(x) + " " + df.format(y) + " " + df.format(z) + " : " + df.format(yaw) + " " + df.format(pitch);
    }

    //serialization
    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> output = new HashMap<String, Object>();
        output.put("x", x);
        output.put("y", y);
        output.put("z", z);
        output.put("yaw", yaw);
        output.put("pitch", pitch);
        return output;
    }

    public static Location deserialize(Map<String, Object> input) {
        return new Location(input);
    }

    public static Location valueOf(Map<String, Object> input) {
        return new Location(input);
    }
}
