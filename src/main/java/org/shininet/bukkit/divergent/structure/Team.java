/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.kitteh.tag.TagAPI;
import org.shininet.bukkit.divergent.DivergentPlugin;

/**
 * @author meiskam
 */
public class Team implements ConfigurationSerializable {
    private String name;
    private ChatColor color;
    private ArrayList<ItemStack> kitItems;
    private ArrayList<ItemStack> skillItems;

    private HashSet<String> players = new HashSet<String>();

    //constructors
    public Team(String name, ChatColor color) {
        this(name, color, new ArrayList<ItemStack>(), new ArrayList<ItemStack>());
    }

    public Team(String name, ChatColor color, ArrayList<ItemStack> kitItems, ArrayList<ItemStack> skillItems) {
        super();
        this.name = name;
        this.color = color;
        this.kitItems = kitItems;
        this.skillItems = skillItems;
    }

    @SuppressWarnings("unchecked")
    public Team(Map<String, Object> input) {
        super();
        try {
            this.name = (String) input.get("name");
            try {
                this.color = ChatColor.valueOf((String) input.get("color"));
            } catch (IllegalArgumentException e) {
                this.color = null;
            }
            this.kitItems = ((ArrayList<ItemStack>) input.get("items-kit"));
            this.skillItems = ((ArrayList<ItemStack>) input.get("items-skill"));
        } catch (Exception e) {
            Bukkit.getLogger().warning("[Divergent] Error deserializing Team");
            e.printStackTrace();
        }
    }

    public void copy(Team teamNew) {
        setName(teamNew.getName());
        setColor(teamNew.getColor());
        setKitItems(teamNew.getKitItems());
        setSkillItems(teamNew.getSkillItems());
    }

    //get-set
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChatColor getColor() {
        return color;
    }

    public void setColor(ChatColor color) {
        if (this.color != color) {
            this.color = color;
            for (String playerName : players) {
                Player player = Bukkit.getPlayer(playerName);
                if (player == null) {
                    continue;
                }
                if (player.getWorld() == DivergentPlugin.inst().getConfigManager().getWorld()) {
                    TagAPI.refreshPlayer(player);
                }
            }
        }
    }

    public ArrayList<ItemStack> getKitItems() {
        return kitItems;
    }

    public ItemStack[] getKitItemsArray() {
        if (getKitItems() == null) {
            return null;
        }
        return getKitItems().toArray(new ItemStack[getKitItems().size()]);
    }

    public ArrayList<ItemStack> getSkillItems() {
        return skillItems;
    }

    public void setKitItems(ArrayList<ItemStack> kitItems) {
        this.kitItems = kitItems;
    }

    public void setSkillItems(ArrayList<ItemStack> skillItems) {
        this.skillItems = skillItems;
    }

    //methods
    public void addPlayer(Player player) {
        players.add(player.getName());
        DivergentPlugin.inst().playerJoinTeam2(player, this);
    }

    public boolean removePlayer(Player player) {
        if (!players.remove(player.getName())) {
            return false;
        }
        return DivergentPlugin.inst().playerLeaveTeam2(player);
    }

    public void bootPlayers(String message) {
        Iterator<String> iter = players.iterator();
        while (iter.hasNext()) {
            String playerName = iter.next();
            Player player = Bukkit.getPlayer(playerName);
            if (player == null) {
                iter.remove();
                continue;
            }
            if (message != null) {
                player.sendMessage(ChatColor.GOLD + message);
            }
            DivergentPlugin.inst().playerLeaveTeam2(player);
            iter.remove();
        }
    }

    //serialization
    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> output = new HashMap<String, Object>();
        output.put("name", name);
        output.put("color", color.name());
        output.put("items-kit", kitItems);
        output.put("items-skill", skillItems);
        return output;
    }

    public static Team deserialize(Map<String, Object> input) {
        return new Team(input);
    }

    public static Team valueOf(Map<String, Object> input) {
        return new Team(input);
    }
}
