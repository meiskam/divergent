/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;

/**
 * @author meiskam
 */
public class ChestGroup implements ConfigurationSerializable {
    private String name;
    private double rate;
    private ArrayList<ItemStack> items;

    //constructors
    public ChestGroup(String name, double rate) {
        this(name, rate, new ArrayList<ItemStack>());
    }

    public ChestGroup(String name, double rate, ArrayList<ItemStack> items) {
        super();
        this.name = name;
        this.rate = rate;
        this.items = items;
    }

    @SuppressWarnings("unchecked")
    public ChestGroup(Map<String, Object> input) {
        super();
        try {
            this.name = (String) input.get("name");
            this.rate = (Double) input.get("rate");
            this.items = ((ArrayList<ItemStack>) input.get("items"));
        } catch (Exception e) {
            Bukkit.getLogger().warning("[Divergent] Error deserializing ChestGroup");
            e.printStackTrace();
        }
    }

    //dynamic get
    public ItemStack getRandItem(Random rand) {
        return items.get(rand.nextInt(items.size()));
    }

    //get-set
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public ArrayList<ItemStack> getItems() {
        return items;
    }

    public void setItems(ArrayList<ItemStack> items) {
        this.items = items;
    }

    //serialization
    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> output = new HashMap<String, Object>();
        output.put("name", name);
        output.put("rate", rate);
        output.put("items", items);
        return output;
    }

    public static ChestGroup deserialize(Map<String, Object> input) {
        return new ChestGroup(input);
    }

    public static ChestGroup valueOf(Map<String, Object> input) {
        return new ChestGroup(input);
    }
}
