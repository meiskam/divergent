/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent.command;

import java.util.ArrayList;
import java.util.Iterator;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.shininet.bukkit.divergent.DivergentPlugin;
import org.shininet.bukkit.divergent.structure.Arena;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.Console;
import com.sk89q.minecraft.util.commands.NestedCommand;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.LocalWorld;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.regions.CuboidRegionSelector;
import com.sk89q.worldedit.regions.Region;

/**
 * @author meiskam
 */
public class ArenaCommand {

    private final DivergentPlugin plugin;

    public ArenaCommand(DivergentPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(
            aliases = { "spawn" },
            desc = "arena spawn management")
    @CommandPermissions("divergent.arena.spawn")
    @NestedCommand(ArenaSpawnCommand.class)
    public void spawn(CommandContext args, CommandSender sender) {
    }

    @Command(
            aliases = { "create" },
            desc = "creates an arena based on worldedit selection",
            usage = "<name>",
            min = 1,
            max = 1)
    @CommandPermissions("divergent.arena.create")
    public void create(CommandContext args, CommandSender sender) throws CommandException {
        Player player = plugin.checkPlayer(sender);
        String arenaName = args.getString(0);

        Arena arena = plugin.getArena(arenaName);
        if (arena != null) {
            sender.sendMessage(ChatColor.RED + "An arena by that name already exists");
            return;
        }

        CuboidClipboard clipboard = getPlayerSelectionToClipboard(player);
        if (clipboard == null) {
            player.sendMessage(ChatColor.RED + "Error creating area, IncompleteRegionException");
            return;
        }

        plugin.getConfigManager().getArenas().add(new Arena(arenaName, clipboard));
        plugin.getConfigManager().saveArenas();

        player.sendMessage(ChatColor.GREEN + "Arena created");
    }

    private CuboidClipboard getPlayerSelectionToClipboard(Player player) {
        LocalSession session = plugin.getWorldEditPlugin().getSession(player);

        Region region;
        Vector min, max;
        try {
            region = session.getSelection(session.getSelectionWorld());
            min = region.getMinimumPoint();
            max = region.getMaximumPoint();
        } catch (IncompleteRegionException e) {
            plugin.getLogger().warning("[Divergent] Error creating arena:");
            e.printStackTrace();
            return null;
        }

        CuboidClipboard clipboard = new CuboidClipboard(max.subtract(min).add(Vector.ONE), min, Vector.ZERO);
        clipboard.copy(plugin.getSession());

        return clipboard;
    }

    private void setPlayerSelection(Player player, Arena arena) {
        CuboidRegion cuboidRegion = arena.getRegion();
        LocalWorld localWorld = BukkitUtil.getLocalWorld(plugin.getConfigManager().getWorld());
        CuboidRegionSelector selector = new CuboidRegionSelector(localWorld, cuboidRegion.getPos1(), cuboidRegion.getPos2());

        plugin.getWorldEditPlugin().getSession(player).setRegionSelector(localWorld, selector);
    }

    @Command(
            aliases = { "save", "update" },
            desc = "saves the blocks in an arena",
            usage = "[name]",
            min = 0,
            max = 1)
    @CommandPermissions("divergent.arena.save")
    @Console
    public void save(CommandContext args, CommandSender sender) throws CommandException {
        Arena arena;
        if (args.argsLength() == 1) {
            String arenaName = args.getString(0);
            arena = plugin.getArena(arenaName);
            if (arena == null) {
                sender.sendMessage(ChatColor.RED + "An arena by that name could not be found");
                return;
            }
        } else {
            Player player = plugin.checkPlayer(sender);
            arena = plugin.wherePlayer(player);
            if (arena == null) {
                player.sendMessage(ChatColor.RED + "You aren't in an arena");
                return;
            }
        }
        if (arena.isEnabled()) {
            sender.sendMessage(ChatColor.RED + "The arena must be disabled to save it");
            return;
        }
        arena.updateCuboid();
        sender.sendMessage(ChatColor.GREEN + "Arena saved");
    }

    @Command(
            aliases = { "delete" },
            desc = "deletes an arena",
            usage = "[name]",
            min = 0,
            max = 1)
    @CommandPermissions("divergent.arena.delete")
    @Console
    public void delete(CommandContext args, CommandSender sender) throws CommandException {
        Arena arena;
        if (args.argsLength() == 1) {
            String arenaName = args.getString(0);
            arena = plugin.getArena(arenaName);
            if (arena == null) {
                sender.sendMessage(ChatColor.RED + "An arena by that name could not be found");
                return;
            }
        } else {
            Player player = plugin.checkPlayer(sender);
            arena = plugin.wherePlayer(player);
            if (arena == null) {
                player.sendMessage(ChatColor.RED + "You aren't in an arena");
                return;
            }
        }
        arena.setEnabled(false);
        arena.delete();
        plugin.getConfigManager().getArenas().remove(arena);
        plugin.getConfigManager().saveArenas();
        sender.sendMessage(ChatColor.GREEN + "Arena deleted");
    }

    @Command(
            aliases = { "resize", "redefine" },
            desc = "sets the arena boundry to your current selection",
            usage = "[name]",
            min = 0,
            max = 1)
    @CommandPermissions("divergent.arena.resize")
    public void resize(CommandContext args, CommandSender sender) throws CommandException {
        Player player = plugin.checkPlayer(sender);
        Arena arena;
        if (args.argsLength() == 1) {
            String arenaName = args.getString(0);
            arena = plugin.getArena(arenaName);
            if (arena == null) {
                player.sendMessage(ChatColor.RED + "An arena by that name could not be found");
                return;
            }
        } else {
            arena = plugin.wherePlayer(player);
            if (arena == null) {
                player.sendMessage(ChatColor.RED + "You aren't in an arena");
                return;
            }
        }
        if (arena.isEnabled()) {
            player.sendMessage(ChatColor.RED + "The arena must be disabled to resize it");
            return;
        }

        CuboidClipboard clipboard = getPlayerSelectionToClipboard(player);
        if (clipboard == null) {
            player.sendMessage(ChatColor.RED + "Error creating area, IncompleteRegionException");
            return;
        }
        arena.setCuboid(clipboard);
        player.sendMessage(ChatColor.GREEN + "Arena resized to your current selection");
    }

    @Command(
            aliases = { "select" },
            desc = "sets your current selection to the arena boundry",
            usage = "[name]",
            min = 0,
            max = 1)
    @CommandPermissions("divergent.arena.select")
    public void select(CommandContext args, CommandSender sender) throws CommandException {
        Player player = plugin.checkPlayer(sender);
        Arena arena;
        if (args.argsLength() == 1) {
            String arenaName = args.getString(0);
            arena = plugin.getArena(arenaName);
            if (arena == null) {
                player.sendMessage(ChatColor.RED + "An arena by that name could not be found");
                return;
            }
        } else {
            arena = plugin.wherePlayer(player);
            if (arena == null) {
                player.sendMessage(ChatColor.RED + "You aren't in an arena");
                return;
            }
        }

        setPlayerSelection(player, arena);
        player.sendMessage(ChatColor.GREEN + "Your selection has been set to the arena boundry");
    }

    @Command(
            aliases = { "list" },
            desc = "lists all arenas",
            max = 0)
    @CommandPermissions("divergent.arena.list")
    @Console
    public void list(CommandContext args, CommandSender sender) throws CommandException {
        ArrayList<Arena> arenas = plugin.getConfigManager().getArenas();
        if (arenas.size() == 0) {
            sender.sendMessage(ChatColor.GOLD + "There are no arenas");
            return;
        }
        Iterator<Arena> iter = arenas.iterator();
        StringBuilder sb = new StringBuilder();
        Arena arena = iter.next();
        if (arena.isEnabled()) {
            if (arena.isRunning()) {
                sb.append(ChatColor.RED);
            } else {
                sb.append(ChatColor.GREEN);
            }
        } else {
            sb.append(ChatColor.BLACK);
        }
        sb.append(arena.getName());
        while (iter.hasNext()) {
            arena = iter.next();
            sb.append(ChatColor.GOLD + ", ");
            if (arena.isEnabled()) {
                if (arena.isRunning()) {
                    sb.append(ChatColor.RED);
                } else {
                    sb.append(ChatColor.GREEN);
                }
            } else {
                sb.append(ChatColor.BLACK);
            }
            sb.append(arena.getName());
        }
        sender.sendMessage(ChatColor.GOLD + "Arenas: " + sb.toString());
    }

    @Command(
            aliases = { "enable" },
            desc = "enables an arena",
            usage = "[name]",
            min = 0,
            max = 1)
    @CommandPermissions("divergent.arena.enable")
    @Console
    public void enable(CommandContext args, CommandSender sender) throws CommandException {
        Arena arena;
        if (args.argsLength() == 1) {
            String arenaName = args.getString(0);
            arena = plugin.getArena(arenaName);
            if (arena == null) {
                sender.sendMessage(ChatColor.RED + "An arena by that name could not be found");
                return;
            }
        } else {
            Player player = plugin.checkPlayer(sender);
            arena = plugin.wherePlayer(player);
            if (arena == null) {
                player.sendMessage(ChatColor.RED + "You aren't in an arena");
                return;
            }
        }
        if (arena.setEnabled(true)) {
            sender.sendMessage(ChatColor.GREEN + "Arena enabled");
            plugin.getConfigManager().saveArenas();
        } else {
            sender.sendMessage(ChatColor.RED + "Arena could not be enabled");
        }
    }

    @Command(
            aliases = { "disable" },
            desc = "disables an arena",
            usage = "[name]",
            min = 0,
            max = 1)
    @CommandPermissions("divergent.arena.disable")
    @Console
    public void disable(CommandContext args, CommandSender sender) throws CommandException {
        Arena arena;
        if (args.argsLength() == 1) {
            String arenaName = args.getString(0);
            arena = plugin.getArena(arenaName);
            if (arena == null) {
                sender.sendMessage(ChatColor.RED + "An arena by that name could not be found");
                return;
            }
        } else {
            Player player = plugin.checkPlayer(sender);
            arena = plugin.wherePlayer(player);
            if (arena == null) {
                player.sendMessage(ChatColor.RED + "You aren't in an arena");
                return;
            }
        }
        arena.setEnabled(false);
        plugin.getConfigManager().saveArenas();
        sender.sendMessage(ChatColor.GREEN + "Arena disabled");
    }

    @Command(
            aliases = { "here" },
            desc = "displays what arena you're standing in",
            max = 0)
    @CommandPermissions("divergent.arena.here")
    public void here(CommandContext args, CommandSender sender) throws CommandException {
        Player player = plugin.checkPlayer(sender);
        Arena arena = plugin.wherePlayer(player);
        if (arena == null) {
            player.sendMessage(ChatColor.RED + "You aren't in an arena");
            return;
        }
        player.sendMessage(ChatColor.GREEN + "You're inside " + arena.getName());
    }

    @Command(
            aliases = { "reset" },
            desc = "reset an arena",
            usage = "[name]",
            min = 0,
            max = 1)
    @CommandPermissions("divergent.arena.reset")
    @Console
    public void reset(CommandContext args, CommandSender sender) throws CommandException {
        Arena arena;
        if (args.argsLength() == 1) {
            String arenaName = args.getString(0);
            arena = plugin.getArena(arenaName);
            if (arena == null) {
                sender.sendMessage(ChatColor.RED + "An arena by that name could not be found");
                return;
            }
        } else {
            Player player = plugin.checkPlayer(sender);
            arena = plugin.wherePlayer(player);
            if (arena == null) {
                player.sendMessage(ChatColor.RED + "You aren't in an arena");
                return;
            }
        }
        arena.reset();
        sender.sendMessage(ChatColor.GREEN + "Arena reset");
    }
}
