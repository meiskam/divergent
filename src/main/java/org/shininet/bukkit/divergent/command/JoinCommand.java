/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent.command;

import java.util.Iterator;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.shininet.bukkit.divergent.DivergentPlugin;
import org.shininet.bukkit.divergent.structure.Arena;
import org.shininet.bukkit.divergent.structure.Team;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;

/**
 * @author meiskam
 */
public class JoinCommand {

    private final DivergentPlugin plugin;

    public JoinCommand(DivergentPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(
            aliases = { "team" },
            desc = "join team",
            min = 0,
            max = 1)
    public void team(CommandContext args, CommandSender sender) throws CommandException {
        Player player = plugin.checkPlayer(sender);
        if (args.argsLength() == 0) {
            Iterator<Team> iter = plugin.getConfigManager().getTeams().iterator();
            StringBuilder sb = new StringBuilder();
            Team team;
            ChatColor color;
            boolean first = true;
            while (iter.hasNext()) {
                if (first) {
                    first = false;
                } else {
                    sb.append(ChatColor.GOLD + ", ");
                }
                team = iter.next();
                color = team.getColor();
                if (color != null) {
                    sb.append(color);
                } else {
                    sb.append(ChatColor.RESET);
                }
                sb.append(team.getName());
            }
            sender.sendMessage(ChatColor.GOLD + "Teams: " + (sb.length() == 0 ? ChatColor.RED + "none" : sb.toString()));
            return;
        }
        String teamName = args.getString(0);
        Team team = plugin.getTeam(teamName);
        if (team == null) {
            player.sendMessage(ChatColor.RED + "That team does not exist");
            return;
        }
        plugin.playerJoinTeam(player, team);
    }

    @Command(
            aliases = { "arena" },
            desc = "join arena",
            min = 0,
            max = 1)
    public void arena(CommandContext args, CommandSender sender) throws CommandException {
        Player player = plugin.checkPlayer(sender);
        if (args.argsLength() == 0) {
            Iterator<Arena> iter = plugin.getConfigManager().getArenas().iterator();
            StringBuilder sb = new StringBuilder();
            Arena arena;
            boolean first = true;
            while (iter.hasNext()) {
                arena = iter.next();
                if (arena.isEnabled()) {
                    if (first) {
                        first = false;
                    } else {
                        sb.append(ChatColor.GOLD);
                        sb.append(", ");
                    }
                    if (arena.isRunning()) {
                        sb.append(ChatColor.RED);
                    } else {
                        sb.append(ChatColor.GREEN);
                    }
                    sb.append(arena.getName());
                }
            }
            sender.sendMessage(ChatColor.GOLD + "Arenas: " + (sb.length() == 0 ? ChatColor.RED + "none" : sb.toString()));
            return;
        }
        String arenaName = args.getString(0);
        Arena arena = plugin.getArena(arenaName);
        if (arena == null) {
            player.sendMessage(ChatColor.RED + "That arena does not exist");
            return;
        }
        plugin.playerJoinArena(player, arena);
    }
}
