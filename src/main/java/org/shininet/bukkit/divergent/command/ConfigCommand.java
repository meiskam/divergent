/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent.command;

import org.bukkit.command.CommandSender;
import org.shininet.bukkit.divergent.DivergentPlugin;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.Console;
import com.sk89q.minecraft.util.commands.NestedCommand;

/**
 * @author meiskam
 */
public class ConfigCommand {

    private final DivergentPlugin plugin;

    public ConfigCommand(DivergentPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(
            aliases = { "load" },
            desc = "load configuration")
    @CommandPermissions("divergent.config.load")
    @NestedCommand(ConfigLoadCommand.class)
    @Console
    public void load(CommandContext args, CommandSender sender) {
    }

    @Command(
            aliases = { "save" },
            desc = "save configuration")
    @CommandPermissions("divergent.config.save")
    @NestedCommand(ConfigSaveCommand.class)
    @Console
    public void save(CommandContext args, CommandSender sender) {
    }
}
