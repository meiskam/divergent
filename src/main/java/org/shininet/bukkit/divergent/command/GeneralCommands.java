/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.shininet.bukkit.divergent.DivergentPlugin;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.Console;
import com.sk89q.minecraft.util.commands.NestedCommand;

/**
 * @author meiskam
 */
public class GeneralCommands {

    private final DivergentPlugin plugin;

    public GeneralCommands(DivergentPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(
            aliases = { "divergent", "dv" },
            desc = "Divergent command")
    @NestedCommand(DivergentCommand.class)
    @Console
    public void divergent(CommandContext args, CommandSender sender) {
    }

    @Command(
            aliases = { "join" },
            desc = "Join team or arena")
    @NestedCommand(JoinCommand.class)
    public void join(CommandContext args, CommandSender sender) {
    }

    @Command(
            aliases = { "leave" },
            desc = "Leave an arena",
            min = 0,
            max = 0)
    @CommandPermissions("divergent.leave")
    public void leave(CommandContext args, CommandSender sender) throws CommandException {
        Player player = plugin.checkPlayer(sender);
        plugin.playerLeaveArena(player, true);
    }

    @Command(
            aliases = { "lobby" },
            desc = "Go to the lobby",
            min = 0,
            max = 0)
    @CommandPermissions("divergent.lobby")
    public void lobby(CommandContext args, CommandSender sender) throws CommandException {
        Player player = plugin.checkPlayer(sender);
        if (!plugin.playerLeaveArena(player, false)) {
            plugin.playerToLobby(player, true);
        }
    }
}
