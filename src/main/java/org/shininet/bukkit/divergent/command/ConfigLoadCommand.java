/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent.command;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.shininet.bukkit.divergent.DivergentPlugin;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.Console;

/**
 * @author meiskam
 */
public class ConfigLoadCommand {

    private final DivergentPlugin plugin;

    public ConfigLoadCommand(DivergentPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(
            aliases = { "arenas" },
            desc = "reload arenas",
            max = 0)
    @CommandPermissions("divergent.config.load.arenas")
    @Console
    public void arenas(CommandContext args, CommandSender sender) throws CommandException {
        plugin.getConfigManager().loadArenas();
        sender.sendMessage(ChatColor.GREEN + "Arena config reloaded");
    }

    @Command(
            aliases = { "teams" },
            desc = "reload teams",
            max = 0)
    @CommandPermissions("divergent.config.load.teams")
    @Console
    public void teams(CommandContext args, CommandSender sender) throws CommandException {
        plugin.getConfigManager().loadTeams();
        sender.sendMessage(ChatColor.GREEN + "Team config reloaded");
    }

    @Command(
            aliases = { "settings" },
            desc = "reload settings",
            max = 0)
    @CommandPermissions("divergent.config.load.settings")
    @Console
    public void settings(CommandContext args, CommandSender sender) throws CommandException {
        plugin.getConfigManager().loadConfig();
        sender.sendMessage(ChatColor.GREEN + "Settings reloaded");
    }

    @Command(
            aliases = { "chests" },
            desc = "reload chests",
            max = 0)
    @CommandPermissions("divergent.config.load.chests")
    @Console
    public void chests(CommandContext args, CommandSender sender) throws CommandException {
        plugin.getConfigManager().loadChests();
        sender.sendMessage(ChatColor.GREEN + "Chest config reloaded");
    }

    @Command(
            aliases = { "all" },
            desc = "reload all",
            max = 0)
    @CommandPermissions("divergent.config.load.all")
    @Console
    public void all(CommandContext args, CommandSender sender) throws CommandException {
        arenas(args, sender);
        teams(args, sender);
        settings(args, sender);
        chests(args, sender);
    }
}
