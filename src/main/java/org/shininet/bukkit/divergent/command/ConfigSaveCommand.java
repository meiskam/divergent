/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent.command;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.shininet.bukkit.divergent.DivergentPlugin;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.Console;

/**
 * @author meiskam
 */
public class ConfigSaveCommand {

    private final DivergentPlugin plugin;

    public ConfigSaveCommand(DivergentPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(
            aliases = { "arenas" },
            desc = "save arenas",
            max = 0)
    @CommandPermissions("divergent.config.save.arenas")
    @Console
    public void arenas(CommandContext args, CommandSender sender) throws CommandException {
        plugin.getConfigManager().saveArenas();
        sender.sendMessage(ChatColor.GREEN + "Arena config saved");
    }

    @Command(
            aliases = { "teams" },
            desc = "save teams",
            max = 0)
    @CommandPermissions("divergent.config.save.teams")
    @Console
    public void teams(CommandContext args, CommandSender sender) throws CommandException {
        plugin.getConfigManager().saveTeams();
        sender.sendMessage(ChatColor.GREEN + "Team config saved");
    }

    @Command(
            aliases = { "settings" },
            desc = "save settings",
            max = 0)
    @CommandPermissions("divergent.config.save.settings")
    @Console
    public void settings(CommandContext args, CommandSender sender) throws CommandException {
        plugin.getConfigManager().saveConfig();
        sender.sendMessage(ChatColor.GREEN + "Settings saved");
    }

    @Command(
            aliases = { "chests" },
            desc = "save chests",
            max = 0)
    @CommandPermissions("divergent.config.save.chests")
    @Console
    public void chests(CommandContext args, CommandSender sender) throws CommandException {
        plugin.getConfigManager().saveChests();
        sender.sendMessage(ChatColor.GREEN + "Chest config saved");
    }

    @Command(
            aliases = { "all" },
            desc = "save all",
            max = 0)
    @CommandPermissions("divergent.config.save.all")
    @Console
    public void all(CommandContext args, CommandSender sender) throws CommandException {
        arenas(args, sender);
        teams(args, sender);
        settings(args, sender);
        chests(args, sender);
    }
}
