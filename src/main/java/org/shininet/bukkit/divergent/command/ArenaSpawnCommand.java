/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent.command;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.shininet.bukkit.divergent.DivergentPlugin;
import org.shininet.bukkit.divergent.structure.Arena;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.CommandUsageException;
import com.sk89q.minecraft.util.commands.Console;

/**
 * @author meiskam
 */
public class ArenaSpawnCommand {
    private final DivergentPlugin plugin;

    public ArenaSpawnCommand(DivergentPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(
            aliases = { "list" },
            desc = "list spawns for this arena",
            usage = "[name]",
            min = 0,
            max = 1)
    @CommandPermissions("divergent.arena.spawn.list")
    @Console
    public void list(CommandContext args, CommandSender sender) throws CommandException {
        Arena arena;
        if (args.argsLength() == 1) {
            String arenaName = args.getString(0);
            arena = plugin.getArena(arenaName);
            if (arena == null) {
                sender.sendMessage(ChatColor.RED + "An arena by that name could not be found");
                return;
            }
        } else {
            Player player = plugin.checkPlayer(sender);
            arena = plugin.wherePlayer(player);
            if (arena == null) {
                player.sendMessage(ChatColor.RED + "You aren't in an arena");
                return;
            }
        }
        org.shininet.bukkit.divergent.structure.Location location;
        ArrayList<org.shininet.bukkit.divergent.structure.Location> spawns = arena.getSpawns();
        org.shininet.bukkit.divergent.structure.Location lobby = arena.getLobby();

        sender.sendMessage(ChatColor.GOLD + "Spawn locations for arena " + arena.getName() + ":");
        sender.sendMessage(ChatColor.GOLD + "lobby: " + (lobby == null ? ChatColor.RED + "none" : lobby));
        if (spawns.size() == 0) {
            sender.sendMessage(ChatColor.RED + "no spawn locations");
            return;
        }
        for (int i = 0; i < spawns.size(); i++) {
            location = spawns.get(i);
            sender.sendMessage("" + ChatColor.GOLD + (i + 1) + ": " + location);
        }
    }

    @Command(
            aliases = { "add", "set" },
            desc = "adds a new spawn for this arena",
            usage = "[name] [x y z [yaw pitch]]",
            min = 0,
            max = 6)
    @CommandPermissions("divergent.arena.spawn.add")
    @Console
    public void set(CommandContext args, CommandSender sender) throws CommandException {
        Arena arena;
        int length = args.argsLength();
        int nameSet = 0;
        if ((length == 1) || (length == 4) || (length == 6)) {
            String arenaName = args.getString(0);
            arena = plugin.getArena(arenaName);
            if (arena == null) {
                sender.sendMessage(ChatColor.RED + "An arena by that name could not be found");
                return;
            }
            nameSet = 1;
        } else {
            Player player = plugin.checkPlayer(sender);
            arena = plugin.wherePlayer(player);
            if (arena == null) {
                player.sendMessage(ChatColor.RED + "You aren't in an arena");
                return;
            }
        }
        org.shininet.bukkit.divergent.structure.Location location;
        if (length == 0 + nameSet) {
            Player player = plugin.checkPlayer(sender);
            location = new org.shininet.bukkit.divergent.structure.Location(player.getLocation());
        } else if (length == 3 + nameSet) {
            location = new org.shininet.bukkit.divergent.structure.Location(args.getDouble(0 + nameSet), args.getDouble(1 + nameSet), args.getDouble(2 + nameSet));
        } else if (length == 5 + nameSet) {
            location = new org.shininet.bukkit.divergent.structure.Location(args.getDouble(0 + nameSet), args.getDouble(1 + nameSet), args.getDouble(2 + nameSet), args.getDouble(3 + nameSet), args.getDouble(4 + nameSet));
        } else {
            throw new CommandUsageException("Arguments not understood", "[name] [x y z [yaw pitch]]");
        }

        arena.getSpawns().add(location);
        plugin.getConfigManager().saveArenas();
        sender.sendMessage(ChatColor.GREEN + "Added spawn #" + arena.getSpawns().size() + " for " + arena.getName() + " at " + location);
    }

    @Command(
            aliases = { "setlobby" },
            desc = "set the lobby for this arena",
            usage = "[name] [x y z [yaw pitch]]",
            min = 0,
            max = 6)
    @CommandPermissions("divergent.arena.spawn.setlobby")
    @Console
    public void setlobby(CommandContext args, CommandSender sender) throws CommandException {
        Arena arena;
        int length = args.argsLength();
        int nameSet = 0;
        if ((length == 1) || (length == 4) || (length == 6)) {
            String arenaName = args.getString(0);
            arena = plugin.getArena(arenaName);
            if (arena == null) {
                sender.sendMessage(ChatColor.RED + "An arena by that name could not be found");
                return;
            }
            nameSet = 1;
        } else {
            Player player = plugin.checkPlayer(sender);
            arena = plugin.wherePlayer(player);
            if (arena == null) {
                player.sendMessage(ChatColor.RED + "You aren't in an arena");
                return;
            }
        }
        org.shininet.bukkit.divergent.structure.Location location;
        if (length == 0 + nameSet) {
            Player player = plugin.checkPlayer(sender);
            location = new org.shininet.bukkit.divergent.structure.Location(player.getLocation());
        } else if (length == 3 + nameSet) {
            location = new org.shininet.bukkit.divergent.structure.Location(args.getDouble(0 + nameSet), args.getDouble(1 + nameSet), args.getDouble(2 + nameSet));
        } else if (length == 5 + nameSet) {
            location = new org.shininet.bukkit.divergent.structure.Location(args.getDouble(0 + nameSet), args.getDouble(1 + nameSet), args.getDouble(2 + nameSet), args.getDouble(3 + nameSet), args.getDouble(4 + nameSet));
        } else {
            throw new CommandUsageException("Arguments not understood", "[name] [x y z [yaw pitch]]");
        }

        arena.setLobby(location);
        plugin.getConfigManager().saveArenas();
        sender.sendMessage(ChatColor.GREEN + "Set the lobby for " + arena.getName() + " at " + location);
    }

    @Command(
            aliases = { "delete" },
            desc = "delete spawn",
            usage = "[name] <spawnid|all>",
            min = 1,
            max = 2)
    @CommandPermissions("divergent.arena.spawn.delete")
    @Console
    public void delete(CommandContext args, CommandSender sender) throws CommandException {
        Arena arena;
        int nameSet = 0;
        if (args.argsLength() == 2) {
            String arenaName = args.getString(0);
            arena = plugin.getArena(arenaName);
            if (arena == null) {
                sender.sendMessage(ChatColor.RED + "An arena by that name could not be found");
                return;
            }
            nameSet = 1;
        } else {
            Player player = plugin.checkPlayer(sender);
            arena = plugin.wherePlayer(player);
            if (arena == null) {
                player.sendMessage(ChatColor.RED + "You aren't in an arena");
                return;
            }
        }

        if (args.getString(0 + nameSet).equalsIgnoreCase("all")) {
            arena.getSpawns().clear();
            plugin.getConfigManager().saveArenas();
            sender.sendMessage(ChatColor.GREEN + "All spawns deleted in " + arena.getName());
            return;
        } else {
            int id = args.getInteger(0 + nameSet);
            ArrayList<org.shininet.bukkit.divergent.structure.Location> spawns = arena.getSpawns();

            if (spawns.size() < id) {
                sender.sendMessage(ChatColor.RED + "That spawn id does not exist");
                return;
            } else {
                spawns.remove(id - 1);
                plugin.getConfigManager().saveArenas();
                sender.sendMessage(ChatColor.GREEN + "Spawn #" + id + " deleted in " + arena.getName());
                return;
            }
        }
    }
}
