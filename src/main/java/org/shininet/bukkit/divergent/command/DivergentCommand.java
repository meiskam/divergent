/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent.command;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.shininet.bukkit.divergent.DivergentPlugin;
import org.shininet.bukkit.divergent.structure.Arena;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissions;
import com.sk89q.minecraft.util.commands.Console;
import com.sk89q.minecraft.util.commands.NestedCommand;

/**
 * @author meiskam
 */
public class DivergentCommand {

    private final DivergentPlugin plugin;

    public DivergentCommand(DivergentPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(
            aliases = { "arena" },
            desc = "arena management")
    @CommandPermissions("divergent.arena")
    @NestedCommand(ArenaCommand.class)
    @Console
    public void arena(CommandContext args, CommandSender sender) {
    }

    @Command(
            aliases = { "setgloballobby" },
            desc = "set the spawn point for dead tributes",
            usage = "[x y z [yaw pitch]]",
            min = 0,
            max = 5)
    @CommandPermissions("divergent.setgloballobby")
    @Console
    public void setgloballobby(CommandContext args, CommandSender sender) throws CommandException {
        org.shininet.bukkit.divergent.structure.Location location;
        if (args.argsLength() == 5) {
            location = new org.shininet.bukkit.divergent.structure.Location(args.getDouble(0), args.getDouble(1), args.getDouble(2), args.getDouble(3), args.getDouble(4));
        } else if (args.argsLength() == 3) {
            location = new org.shininet.bukkit.divergent.structure.Location(args.getDouble(0), args.getDouble(1), args.getDouble(2));
        } else {
            Player player = plugin.checkPlayer(sender);
            location = new org.shininet.bukkit.divergent.structure.Location(player.getLocation());
            plugin.getConfigManager().setWorld(player.getWorld());
        }
        plugin.getConfigManager().setLobbySpawn(location);
        sender.sendMessage(ChatColor.GREEN + "Set global lobby spawn to: " + location);
    }

    @Command(
            aliases = { "disableall" },
            desc = "disabled every arena at once",
            max = 0)
    @CommandPermissions("divergent.disableall")
    @Console
    public void disable(CommandContext args, CommandSender sender) throws CommandException {
        for (Arena arena : plugin.getConfigManager().getArenas()) {
            arena.setEnabled(false);
        }
        sender.sendMessage(ChatColor.GREEN + "All arenas disabled");
    }

    @Command(
            aliases = { "enableall" },
            desc = "enable every arena at once",
            max = 0)
    @CommandPermissions("divergent.enableall")
    @Console
    public void enable(CommandContext args, CommandSender sender) throws CommandException {
        boolean error = false;

        for (Arena arena : plugin.getConfigManager().getArenas()) {
            if (!arena.setEnabled(true)) {
                error = true;
            }
        }
        if (error) {
            sender.sendMessage(ChatColor.RED + "Attempted to enable all arenas, some didn't successfully enable");
        } else {
            sender.sendMessage(ChatColor.GREEN + "All arenas enabled");
        }
    }

    @Command(
            aliases = { "start" },
            desc = "manually start an arena",
            usage = "[name] [time]",
            min = 0,
            max = 2)
    @CommandPermissions("divergent.start")
    @Console
    public void start(CommandContext args, CommandSender sender) throws CommandException {
        Arena arena;
        int time = 5;
        String arenaName;
        if (args.argsLength() == 0) {
            Player player = plugin.checkPlayer(sender);
            arena = plugin.wherePlayer(player);
            if (arena == null) {
                player.sendMessage(ChatColor.RED + "You aren't in an arena");
                return;
            }
        } else {
            arenaName = args.getString(0);
            arena = plugin.getArena(arenaName);
        }
        if (args.argsLength() == 2) {
            time = args.getInteger(1);
        }
        if (arena == null) {
            sender.sendMessage(ChatColor.RED + "That arena doesn't exist");
            return;
        }
        if (!arena.isEnabled()) {
            sender.sendMessage(ChatColor.RED + "Arena " + arena.getName() + " is not enabled");
            return;
        }
        if (arena.isRunning()) {
            sender.sendMessage(ChatColor.RED + "Arena " + arena.getName() + " is already running");
            return;
        }
        sender.sendMessage(ChatColor.GREEN + "Attempting to start arena " + arena.getName() + " in " + time + " seconds");
        arena.startTimer(time, true);
    }

    @Command(
            aliases = { "stop" },
            desc = "manually stop an arena",
            usage = "[name]",
            min = 0,
            max = 1)
    @CommandPermissions("divergent.stop")
    @Console
    public void stop(CommandContext args, CommandSender sender) throws CommandException {
        Arena arena;
        String arenaName;
        if (args.argsLength() == 0) {
            Player player = plugin.checkPlayer(sender);
            arena = plugin.wherePlayer(player);
            if (arena == null) {
                player.sendMessage(ChatColor.RED + "You aren't in an arena");
                return;
            }
        } else {
            arenaName = args.getString(0);
            arena = plugin.getArena(arenaName);
        }
        if (arena == null) {
            sender.sendMessage(ChatColor.RED + "That arena doesn't exist");
            return;
        }
        if (!arena.isEnabled()) {
            sender.sendMessage(ChatColor.RED + "Arena " + arena.getName() + " is not enabled");
            return;
        }
        if (!arena.isRunning()) {
            sender.sendMessage(ChatColor.RED + "Arena " + arena.getName() + " is not running");
            return;
        }
        sender.sendMessage(ChatColor.GREEN + "Attempting to stop arena " + arena.getName());
        arena.setRunning(false, "Match has been stopped");
    }

    @Command(
            aliases = { "config" },
            desc = "configuration")
    @CommandPermissions("divergent.config")
    @NestedCommand(ConfigCommand.class)
    @Console
    public void config(CommandContext args, CommandSender sender) {
    }

    /*@Command(aliases = {"temp"}, desc = "temporary", max = 0) //TODO add a way to properly add chests and teams
    @Console
    public void temp(CommandContext args, CommandSender sender) throws CommandException {
        plugin.temp.get(0).getItems().add(((Player)sender).getItemInHand());
        plugin.getConfig().set("tempChest", plugin.temp);
        plugin.saveConfig();
    }*/
}
