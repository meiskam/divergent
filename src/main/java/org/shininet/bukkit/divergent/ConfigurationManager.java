/*
 * Divergent
 * Copyright (C) 2014 ShiniNet <http://www.shininet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.shininet.bukkit.divergent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.shininet.bukkit.divergent.structure.Arena;
import org.shininet.bukkit.divergent.structure.ChestGroup;
import org.shininet.bukkit.divergent.structure.Team;

/**
 * Represents the global configuration
 * 
 * @author sk89q
 * @author Michael
 * @author meiskam
 */
public class ConfigurationManager {
    /**
     * Reference to the plugin.
     */
    private DivergentPlugin plugin;

    //configuration yamls
    private YamlConfiguration configYML;
    private YamlConfiguration chestsYML;
    private YamlConfiguration teamsYML;
    private YamlConfiguration arenasYML;
    private File configFile;
    private File chestsFile;
    private File teamsFile;
    private File arenasFile;

    /**
     * Construct the object.
     * 
     * @param plugin The plugin instance
     */
    public ConfigurationManager(DivergentPlugin plugin) {
        this.plugin = plugin;
    }

    //variables
    private String world;
    private int playerMin;
    private int playerMax;
    private boolean opPermissions;
    private int maxArenaSize;
    private org.shininet.bukkit.divergent.structure.Location lobbySpawn;
    private int maxChestItems;
    private ArrayList<ChestGroup> chests;
    private ArrayList<Arena> arenas;
    private ArrayList<Team> teams;

    //set-get
    public String getWorldName() {
        return world;
    }

    public void setWorldName(String worldName) {
        this.world = worldName;
        saveConfig();
    }

    public World getWorld() {
        return Bukkit.getWorld(getWorldName());
    }

    public void setWorld(World world) {
        setWorldName(world.getName());
    }

    public int getPlayerMin() {
        return playerMin;
    }

    public void setPlayerMin(int playerMin) {
        this.playerMin = playerMin;
        saveConfig();
    }

    public int getPlayerMax() {
        return playerMax;
    }

    public void setPlayerMax(int playerMax) {
        this.playerMax = playerMax;
        saveConfig();
    }

    public boolean isOpPermissions() {
        return opPermissions;
    }

    public void setOpPermissions(boolean opPermissions) {
        this.opPermissions = opPermissions;
        saveConfig();
    }

    public int getMaxArenaSize() {
        return maxArenaSize;
    }

    public void setMaxArenaSize(int maxArenaSize) {
        this.maxArenaSize = maxArenaSize;
        saveConfig();
    }

    public org.shininet.bukkit.divergent.structure.Location getLobbySpawn() {
        return lobbySpawn;
    }

    public void setLobbySpawn(org.shininet.bukkit.divergent.structure.Location lobbySpawn) {
        this.lobbySpawn = lobbySpawn;
        saveConfig();
    }

    public int getMaxChestItems() {
        return maxChestItems;
    }

    public void setMaxChestItems(int maxChestItems) {
        this.maxChestItems = maxChestItems;
        saveConfig();
    }

    public ArrayList<ChestGroup> getChests() {
        return chests;
    }

    public ArrayList<Arena> getArenas() {
        return arenas;
    }

    public ArrayList<Team> getTeams() {
        return teams;
    }

    //dynamic config
    public double getChestWeight() {
        double output = 0;
        for (ChestGroup chest : chests) {
            output += chest.getRate();
        }
        return output;
    }

    //save-load config
    public boolean saveAll() {
        boolean output = true;
        if (!saveConfig())
            output = false;
        if (!saveChests())
            output = false;
        if (!saveTeams())
            output = false;
        if (!saveArenas())
            output = false;
        return output;
    }

    public void loadAll() {
        loadConfig();
        loadChests();
        loadTeams();
        loadArenas();
    }

    public boolean saveConfig() {
        //configYML.setHeader(CONFIG_HEADER);

        configYML.set("world-name", world);
        configYML.set("arena-player-min", playerMin);
        configYML.set("arena-player-max", playerMax);
        configYML.set("op-permissions", opPermissions);
        configYML.set("max-arena-size", maxArenaSize);
        configYML.set("chest-items-max", maxChestItems);
        configYML.set("global-lobby-spawn", lobbySpawn);

        try {
            configYML.save(configFile);
        } catch (IOException e) {
            plugin.getLogger().warning("[Divergent] Error saving config");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void loadConfig() {
        if (configFile == null) {
            configFile = new File(plugin.getDataFolder(), "config.yml");
        }
        configYML = YamlConfiguration.loadConfiguration(configFile);

        world = configYML.getString("world-name", "world");
        playerMin = configYML.getInt("arena-player-min", 12);
        playerMax = configYML.getInt("arena-player-max", 24);
        opPermissions = configYML.getBoolean("op-permissions", true);
        maxArenaSize = configYML.getInt("max-arena-size", 1000000);
        maxChestItems = configYML.getInt("chest-items-max", 8);
        try {
            lobbySpawn = (org.shininet.bukkit.divergent.structure.Location) configYML.get("global-lobby-spawn");
        } catch (ClassCastException e) {
            lobbySpawn = null;
        }

        plugin.onConfigLoad();
    }

    public boolean saveChests() {
        chestsYML.set("chests", chests);

        try {
            chestsYML.save(chestsFile);
        } catch (IOException e) {
            plugin.getLogger().warning("[Divergent] Error saving chests");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    public void loadChests() {
        if (chestsFile == null) {
            chestsFile = new File(plugin.getDataFolder(), "chests.yml");
        }
        chestsYML = YamlConfiguration.loadConfiguration(chestsFile);

        chests = (ArrayList<ChestGroup>) chestsYML.get("chests");
        if (chests == null) {
            chests = new ArrayList<ChestGroup>();
        }
    }

    public boolean saveTeams() {
        teamsYML.set("teams", teams);

        try {
            teamsYML.save(teamsFile);
        } catch (IOException e) {
            plugin.getLogger().warning("[Divergent] Error saving teams");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    public void loadTeams() {
        if (teamsFile == null) {
            teamsFile = new File(plugin.getDataFolder(), "teams.yml");
        }
        teamsYML = YamlConfiguration.loadConfiguration(teamsFile);

        ArrayList<Team> teamsNew = (ArrayList<Team>) teamsYML.get("teams");
        if (teams == null) {
            if (teamsNew == null) {
                teams = new ArrayList<Team>();
            } else {
                teams = teamsNew;
            }
            return;
        }
        if (teamsNew == null) {
            plugin.leaveAllTeams();
            teams = new ArrayList<Team>();
            return;
        }
        Iterator<Team> teamIter = teams.iterator();
        while (teamIter.hasNext()) { //check if we need to delete any old teams
            Team team = teamIter.next();
            boolean teamFound = false;
            for (Team teamNew : teamsNew) {
                if (team.getName().equalsIgnoreCase(teamNew.getName())) {
                    teamFound = true;
                    break;
                }
            }
            if (teamFound == false) {
                team.bootPlayers("Your team no longer exists");
                teamIter.remove();
            }
        }
        for (Team teamNew : teamsNew) { //check if we need to modify old teams, or add new ones
            boolean teamFound = false;
            for (Team team : teams) {
                if (team.getName().equalsIgnoreCase(teamNew.getName())) { //update old team
                    team.copy(teamNew);
                    teamFound = true;
                    break;
                }
            }
            if (teamFound == false) {
                teams.add(teamNew);
            }
        }
        saveTeams();
    }

    public boolean saveArenas() {
        arenasYML.set("arenas", arenas);

        try {
            arenasYML.save(arenasFile);
        } catch (IOException e) {
            plugin.getLogger().warning("[Divergent] Error saving arenas");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    public void loadArenas() {
        if (arenasFile == null) {
            arenasFile = new File(plugin.getDataFolder(), "arenas.yml");
        }
        arenasYML = YamlConfiguration.loadConfiguration(arenasFile);

        ArrayList<Arena> arenasNew = (ArrayList<Arena>) arenasYML.get("arenas");
        if (arenas == null) {
            if (arenasNew == null) {
                arenas = new ArrayList<Arena>();
            } else {
                arenas = arenasNew;
            }
            return;
        }
        if (arenasNew == null) {
            plugin.stopAllArenas();
            arenas = new ArrayList<Arena>();
            return;
        }
        Iterator<Arena> arenaIter = arenas.iterator();
        while (arenaIter.hasNext()) { //check if we need to delete any old arenas
            Arena arena = arenaIter.next();
            boolean arenaFound = false;
            for (Arena arenaNew : arenasNew) {
                if (arena.getName().equalsIgnoreCase(arenaNew.getName())) {
                    arenaFound = true;
                    break;
                }
            }
            if (arenaFound == false) {
                arena.bootPlayers("Your arena no longer exists");
                arena.delete();
                arenaIter.remove();
            }
        }
        for (Arena arenaNew : arenasNew) { //check if we need to modify old arenas, or add new ones
            boolean arenaFound = false;
            for (Arena arena : arenas) {
                if (arena.getName().equalsIgnoreCase(arenaNew.getName())) { //update old arena
                    arena.copy(arenaNew);
                    arenaFound = true;
                    break;
                }
            }
            if (arenaFound == false) {
                arenas.add(arenaNew);
            }
        }
        saveArenas();

        //TODO deal with old arenas
        arenas = (ArrayList<Arena>) arenasYML.get("arenas");
        if (arenas == null) {
            arenas = new ArrayList<Arena>();
        }
    }
}
